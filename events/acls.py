import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    url = "https://api.pexels.com/v1/search?"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    city_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city + "," + state + ",USA",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    response = requests.get(city_url, params=params)
    content = json.loads(response.content)
    # name = content[0]["name"]
    lat = str(content[0]["lat"])
    lon = str(content[0]["lon"])

    param = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_response = requests.get(weather_url, params=param)
    weather = json.loads(weather_response.content)
    weather_data = {
        "temp": weather["main"]["temp"],
        "description": weather["weather"][0]["description"],
    }
    return weather_data
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary

    # url = "http://api.openweathermap.org/geo/1.0/direct?{params}"

    # content = json.loads(response.content)


# current weather data
# https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}

# geocoding
# http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}

# url = 'https://api.github.com/some/endpoint'
# headers = {'Authorization': 'Pexels_API_key'}

# r = requests.get(url, headers=headers)
# r.json()

# What should those functions look like? Each should take a city and state,
# and make API calls using those.
# Use the Open Weather API

# When someone creates a new Location, your code must use the Pexels Search
# for Photos  API to get the url of a picture for the city. Save that url
# as part of the Location instance.

# When someone gets the details of a Conference, your code must get the
# current weather conditions for the city the conference is in.
# This will be a two-step process:
# Use the Geocoding API  to translate the city and state for the conference
# into latitude and longitude.
# Use the Current weather data  API to get the current weather for the
# latitude and longitude found in the previous step.
# These features do not rely on one another. This means you can choose which
# one you want to start with, the picture one or the weather one.
